package controller;

import java.util.ArrayList;
import java.util.Calendar;
import model.Student;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class StudentControl {

    private List<Student> students = new ArrayList<>();
    private int registration = 201901000;

    private int currentYear() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        return year;
    }

    private String generateRegistration() {
        registration++;
        return Integer.toString(registration);
    }

    public void save(Student student) {
        student.setRegistration(generateRegistration());
        student.setStartYear(currentYear());
        student.setSituation("Ativo");
        students.add(student);
        JOptionPane.showMessageDialog(null, "Aluno, " + student.getName() + ", cadastrado com sucesso!");
    }

    public List<Student> getActive() {
        List<Student> studentsActive = new ArrayList<>();
        for (Student student : students) {
            if (student.getSituation().equalsIgnoreCase("Ativo")) {
                studentsActive.add(student);
            }
        }
        return studentsActive;
    }

    public List<Student> getInactive() {
        List<Student> studentsInactive = new ArrayList<>();
        for (Student student : students) {
            if (student.getSituation().equalsIgnoreCase("Inativo")) {
                studentsInactive.add(student);
            }
        }
        return studentsInactive;
    }

    public List<Student> searchByName(String name) {
        List<Student> studentsName = new ArrayList<>();
        for (Student student : students) {
            if (student.getName().equalsIgnoreCase(name)) {
                studentsName.add(student);
            }
        }
        return studentsName;
    }

    public Student searchByRegistration(String registration) {
        Student studentReturn = null;
        for (Student student : students) {
            if (registration.equalsIgnoreCase(student.getRegistration())) {
                studentReturn = student;
                break;
            }
        }
        return studentReturn;
    }

    public void changeSituation(String cpf) {
        for (Student student : students) {
            if (cpf.equals(student.getCpf())) {
                if (student.getSituation().equalsIgnoreCase("Ativo")) {
                    student.setSituation("Inativo");
                } else {
                    student.setSituation("Ativo");
                }
            }
        }
    }

    public List<Student> getStudents() {
        return students;
    }
}
