package view;

import controller.StudentControl;
import java.util.List;
import javax.swing.JOptionPane;
import model.Student;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class ViewStudent {

    public static void main(String[] args) {
        String option;
        StudentControl sC = new StudentControl();
        List<Student> students;
        Student student;
        do {
            option = JOptionPane.showInputDialog("Digite a opção desejada:"
                    + "\n1)Cadastrar Aluno:\n2) Ativar/Inativar Aluno:\n3)Pesquisar por Nome:\n4) Pesquisar por Matricula\n5)Listar Ativos:\n6) Listar Inativos:\n7)Sair");
            switch (option) {
                case "1":
                    student = registerStudent();
                    sC.save(student);
                    break;
                case "2":
                    students = sC.getStudents();
                    listStudent(students);
                    sC.changeSituation(JOptionPane.showInputDialog("Digite o CPF do Aluno"));
                    break;
                case "3":
                    String name = JOptionPane.showInputDialog("Pesquise pelo nome do Aluno");
                    students = sC.searchByName(name);
                    if (students.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Nenhum Aluno Encontrado");
                    } else {
                        listStudent(students);
                    }
                    break;
                case "4":
                    students = sC.getStudents();
                    listStudent(students);
                    student = sC.searchByRegistration(JOptionPane.showInputDialog("Digite a matricula do aluno"));
                    if (student == null) {
                        JOptionPane.showMessageDialog(null, "Não foi encontrado nenhum aluno com essa matricula!");
                    } else {
                        showStudent(student);
                    }
                    break;
                case "5":
                    students = sC.getActive();
                    listStudent(students);
                    break;
                case "6":
                    students = sC.getInactive();
                    listStudent(students);
                    break;
                case "7":
                    System.exit(0);
                    break;
            }
        } while (!"7".equals(option));
    }

    private static Student registerStudent() {
        Student student = new Student();
        student.setName(JOptionPane.showInputDialog("Insira o Nome do Aluno:"));
        student.setCourse(JOptionPane.showInputDialog("Insira o Curso do Aluno:"));
        student.setCpf(JOptionPane.showInputDialog("Insira o CPF do Aluno:"));
        return student;
    }

    private static void listStudent(List<Student> students) {
        for (Student student : students) {
            System.out.println("Nome: " + student.getName());
            System.out.println("Matricula: " + student.getRegistration());
            System.out.println("Ano de Início: " + student.getStartYear());
            System.out.println("Curso: " + student.getCourse());
            System.out.println("CPF: " + student.getCpf());
            System.out.println("Situação: " + student.getSituation());
            System.out.println("");
        }
        System.out.println("");
    }

    private static void showStudent(Student student) {
        System.out.println("Nome: " + student.getName());
        System.out.println("Matricula: " + student.getRegistration());
        System.out.println("Ano de Início: " + student.getStartYear());
        System.out.println("Curso: " + student.getCourse());
        System.out.println("CPF: " + student.getCpf());
        System.out.println("Situação: " + student.getSituation());
        System.out.println("");
    }

}
